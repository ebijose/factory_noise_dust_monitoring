# Noise and Dust Monitoring in Factory Environments

This proof of concept (POC) for Ministry of Manpower (MOM) is to monitor noise and dust within factory environments online. The monitoring unit mainly consits of the following:

    * Raspberry pi 3 single-board computer
    * FC-04 audio board (http://www.continental.sg/products/sound-sensor), which detects sound in the environment.
      - The sound sensitivity is adjustable with the potentiometer on the audio board.
    * ADS1115 16-Bit ADC - 4 Channel with Programmable Gain Amplifier that converts analog audio signals to digital values.
      - The programmable gain was set to 1.
    * Particle sensor (https://sensing.honeywell.com/HPMA115S0-XXX-particle-sensors) which detect and count particle concentration (0 μg/m3 to 1,000 μg/m3) in the environment.
      - Raspberry pi camera module v2 to snap images of the environment.
    * The image can be used to verify whether the workers are wearing proper PPE (mainly face masks, gloves, and ear plugs).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Libraries for ADS1115 ADC and HPMA115S0 particle sensors have to be installed

#### To install the Adafruit ADS1x15 Python library to the raspberry pi.

```
sudo apt-get update
sudo apt-get install build-essential python-dev python-smbus git
cd ~
git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git
cd Adafruit_Python_ADS1x15
sudo python setup.py install
```

#### To install the Honeywell HPMA115S0-XXX particle sensor Python library to the raspberry pi.

Clone or download the repository to the Raspberry Pi. If you have git package installed, simply run:

```
git clone https://github.com/ThingType/HPMA115S0_Python
```

For Raspberry Pi 3, use serial port “/dev/ttyS0“. For older versions of Raspberry Pi, use “/dev/ttyAMA0”


### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
