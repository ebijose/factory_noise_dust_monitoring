# This program will capture audio signals in digitised format (ADC output via I2C).
# If the audio signal level is above certain threshold, then camera will be triggered
# Also, Honeywell HPMA115S0 particle sensor will be monitoring the dust/particle level
# in the factory environments. Both PM2.5 and PM10 will be monitored every minute.
# If the noise level and/or particle density level (PM2.5) is above 70dB and 200 ug/m3
# respectively, then the camera will be triggered and the captured image will be uploaded
# to the database along with noise, PM2.5 and PM10 readings. If the noise and PM2.5 readings
# are below their respective thresholds, no photos will be taken, only the data readings
# will be uploaded to the sensor.

# Author: Ebi Jose
# License: Restricted

# Import the ADS1x15 module.
import Adafruit_ADS1x15
# Import the HPMA115S0 module
import HPMA115S0

from urllib.request import Request, urlopen
from urllib.error import  URLError

import json
import time
import threading
import datetime
import io
import struct
import socket                   	# Import socket module
import os
import logging

from picamera import PiCamera
from datetime import datetime
from datetime import date

MAX_DATA_COUNTER = 5
TIME_INTERVAL_BETWEEN_IMAGES = 30
PSI_THRESHOLD = 200  			# unhealthy level based on wiki
NOISE_THRESHOLD = 80 			# in dB

SLOPE = 0.005754
INTERCEPT = 16.5173

dataCounter = 0
imageIntervalCounter = 0

conditionsSetURL = "http://159.65.134.155:8002/postData"

s = socket.socket()             # Create a socket object
host = '159.65.134.155'   	# Get local machine name
port = 8002                     # Reserve a port for your service.

def upload_data(data):
    params = json.dumps(data).encode('utf8')
    req = Request(conditionsSetURL, data=params, headers={'content-type': 'application/json'})
    # for more info on catching URLError, please read https://docs.python.org/3.4/howto/urllib2.html
    try:
        response = urlopen(req)
    except URLError as e:
        if hasattr(e, 'reason'):
            logger.error('Failed to reach the data server. Reason: %', e.reason)
        elif hasattr(e, 'code'):
            logger.error('The server couldn\'t fulfill the request. Error code: %d', e.code)

def upload_image(filePath):
    try:
        host = '159.65.134.155'
        port = 5000
        s = socket.socket()
        s.connect((host, port))

        # Handshake: Find the file size and send it to the server
        fileSize = os.path.getsize(filePath)
        rawFileSize = struct.pack("!I", fileSize)
        s.send(rawFileSize)

        with open(filePath, 'rb') as f:
            logger.info('Uploading image to the server')
            fileSizeRead = f.read(4096)
            while(fileSizeRead):
                s.send(fileSizeRead)
                fileSizeRead = f.read(4096)
            f.close()
            logger.info('Uploaded picture !')
            os.remove(filePath)
        s.close()
    except socket.error as e:
        logger.error('Error in connecting to the image server, %s', e)

def initialise_particle_sensor():
    hpma115S0 = HPMA115S0.HPMA115S0("/dev/ttyS0")
    hpma115S0.init()
    hpma115S0.startParticleMeasurement()
    return hpma115S0

def initialise_noise_sensor():
    # Create an ADS1115 ADC (16-bit) instance.
    adc = Adafruit_ADS1x15.ADS1115()
    # Chosen gain of 1 for reading voltages from 0 to 4.09V.
    GAIN  = 1
    return adc, GAIN

def get_particle_data():
    if (hpma115S0.readParticleMeasurement()):
         return hpma115S0._pm2_5, hpma115S0._pm10
    else:
        return -1, -1

def get_noise_data():
    adc_val = adc.read_adc(0, gain=GAIN)
    if(adc_val >= 0 and adc_val <= 65536):
        return round( (4*(10**(-11))*(adc_val*adc_val*adc_val))-8*(10**(-7))*(adc_val*adc_val)+0.0075*adc_val+47.574 , 2 )
    else:
        return -1

def capture_image():
    dateTimeNow = str(datetime.now().strftime("%d%m%Y_%H%M%S"))
    filePath =  '/home/pi/Pictures/' + dateTimeNow + '.jpg'
    camera.capture(filePath)
    logger.info('Image saved to %s', filePath)
    return filePath

def initialise_camera():
    camera = PiCamera()
    camera.resolution = (1024, 768)
    camera.rotation = 180
    return camera

try:
    logger = logging.getLogger('/home/pi/noise_and_particle_meas/logs/'+ str(date.today()))
    hdlr = logging.FileHandler('/home/pi/noise_and_particle_meas/logs/'+ str(date.today()) + '.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    logger.info('======= Starting the program ========')

    hpma115S0 = initialise_particle_sensor()
    logger.info('Initialised particle sensor %s', hpma115S0)

    adc, GAIN = initialise_noise_sensor()
    logger.info('Initialised noise sensor %s', adc)

    camera = initialise_camera()
    logger.info('Initialised camera %s', camera)

    count = 0
    pm2_5 = -1
    pm10 = -1

    time.sleep(10)

    while(count < 10000):
        pm2_5, pm10 = get_particle_data()
        if(pm2_5 >= 1):
            break
        count = count + 1

    time.sleep(1)

    hpma115S0.stopParticleMeasurement()

    noiseDB = get_noise_data()
    logger.info('noiseDB = %s, PM 2.5 = %s ug/m3, PM 10 = %s ug/m3', noiseDB, pm2_5, pm10)

    if(pm2_5 != -1 and pm10 != -1 and noiseDB != -1):
        data = {'noise_val':noiseDB, 'pm25_val':pm2_5, 'pm10_val':pm10}
        upload_data(data)

        filePath = capture_image()
        upload_image(filePath)
    else:
        logger.info('Problem reading sensor', 'noise_val:', noiseDB, 'pm25_val:', pm2_5, 'pm10_val:', pm10)

except KeyboardInterrupt:
    logger.info('======= program stopped by user ========')
